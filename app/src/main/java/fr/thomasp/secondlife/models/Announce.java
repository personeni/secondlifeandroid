package fr.thomasp.secondlife.models;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.util.Date;

import fr.thomasp.secondlife.database.converters.DateConverter;

@Entity
public class Announce {

    @PrimaryKey
    private int id;
    private String title;
    private String description;
    private double price;
    @Embedded(prefix = "creator_")
    private User creator;
    @TypeConverters({DateConverter.class})
    private Date createdDate;

    public Announce(int id, String title, String description, double price, User creator, Date createdDate){
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.creator = creator;
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

}
