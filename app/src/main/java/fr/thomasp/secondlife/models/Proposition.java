package fr.thomasp.secondlife.models;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Proposition {
    @PrimaryKey
    private int id;
    @Embedded(prefix = "announce_")
    private Announce announce;
    private Double price;
    @Embedded(prefix = "user_")
    private User user;

    public Proposition(int id,Announce announce, Double price, User user){
        this.id = id;
        this.announce = announce;
        this.price = price;
        this.user = user;
    }
    public Announce getAnnounce() {
        return announce;
    }

    public void setAnnounce(Announce announce) {
        this.announce = announce;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId(){return this.id;}
    public void setId(int id){this.id = id;}

}
