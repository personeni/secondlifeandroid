package fr.thomasp.secondlife;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import org.w3c.dom.Text;

import fr.thomasp.secondlife.api.ApiRequests;
import fr.thomasp.secondlife.ui.GameDetails.PropositionDialog;
import fr.thomasp.secondlife.ui.UserAnnounces.UserAnnouncesFragment;
import fr.thomasp.secondlife.ui.addGame.AddGameFragment;
import fr.thomasp.secondlife.ui.home.HomeFragment;
import fr.thomasp.secondlife.ui.propositions.PropositionsFragment;
import fr.thomasp.secondlife.ui.propositions.PropositionsStatusFragment;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }

    public void clickEditButton(View v){
        ViewSwitcher switcherPhone = v.getRootView().findViewById(R.id.viewSwitcherPhone);
        ViewSwitcher switcherMail = v.getRootView().findViewById(R.id.viewSwitcherMail);
        ViewSwitcher switcherButton = v.getRootView().findViewById(R.id.viewSwitcherEditButton);
        TextView phone = v.getRootView().findViewById(R.id.textViewPhone);
        TextView mail = v.getRootView().findViewById(R.id.textViewMail);
        EditText editPhone = v.getRootView().findViewById(R.id.editTextPhone);
        EditText editMail = v.getRootView().findViewById(R.id.editTextMail);

        editPhone.setText(phone.getText());
        editMail.setText(mail.getText());
        switcherPhone.showNext();
        switcherMail.showNext();
        switcherButton.showNext();
    }
    public void clickValidateAccountInfosButton(View v){
        ViewSwitcher switcherPhone = v.getRootView().findViewById(R.id.viewSwitcherPhone);
        ViewSwitcher switcherMail = v.getRootView().findViewById(R.id.viewSwitcherMail);
        ViewSwitcher switcherButton = v.getRootView().findViewById(R.id.viewSwitcherEditButton);
        TextView phone = v.getRootView().findViewById(R.id.textViewPhone);
        TextView mail = v.getRootView().findViewById(R.id.textViewMail);
        EditText editPhone = v.getRootView().findViewById(R.id.editTextPhone);
        EditText editMail = v.getRootView().findViewById(R.id.editTextMail);

        phone.setText(editPhone.getText());
        mail.setText(editMail.getText());
        switcherPhone.showNext();
        switcherMail.showNext();
        switcherButton.showNext();

        //TODO sauvegarder les modifs
    }
    public void clickPropositionToCheck(View v){
        PropositionsFragment fragment = new PropositionsFragment();
        FragmentTransaction transaction = ((AppCompatActivity)v.getContext()).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.account_fragment,fragment, null)
                .addToBackStack(null)
                .commit();
    }
    public void clickPropositionStatus(View v){
        PropositionsStatusFragment fragment = new PropositionsStatusFragment();
        FragmentTransaction transaction = ((AppCompatActivity)v.getContext()).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.account_fragment,fragment, null)
                .addToBackStack(null)
                .commit();
    }
    public void addGame(View v){
        AddGameFragment fragment = new AddGameFragment();
        FragmentTransaction transaction = ((AppCompatActivity)v.getContext()).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_fragment, fragment, null)
            .addToBackStack(null)
            .commit();
    }
    public void clickMyAnnounces(View v){
        UserAnnouncesFragment fragment = new UserAnnouncesFragment();
        FragmentTransaction transaction = ((AppCompatActivity)v.getContext()).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.account_fragment, fragment, null)
                .addToBackStack(null)
                .commit();
    }
    public void removeAnnounce(View v){
        Log.d("","suppression d'une annonce");
    }

    public void clickAddGame(View v){
        EditText titleEdit = v.getRootView().findViewById(R.id.editTextAnnounceTitle);
        EditText descriptionEdit = v.getRootView().findViewById(R.id.editTextAnnounceDescription);
        EditText priceEdit = v.getRootView().findViewById(R.id.editTextPrice);

        String title = titleEdit.getText().toString();
        String description = descriptionEdit.getText().toString();
        String price = priceEdit.getText().toString();

        MutableLiveData<Boolean> resultOk = new MutableLiveData<>();

        Observer observer = new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean o) {
                if(o){
                    HomeFragment fragment = new HomeFragment();
                    FragmentTransaction transaction = ((AppCompatActivity)v.getContext()).getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.addGameFragment, fragment, null)
                            .addToBackStack(null)
                            .commit();
                }else{
                    Toast.makeText(v.getContext(), "Erreur lors de la création de l'annonce", Toast.LENGTH_SHORT).show();
                }
            }
        };
        resultOk.observeForever(observer);
        if(title.isEmpty() || description.isEmpty() || price.isEmpty()){
            Toast.makeText(v.getContext(), "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
        }else{
            ApiRequests.getInstance().createAnnounce(v.getContext(), title, description, price, resultOk);
        }
    }
    public void showPropositionDialog(View v) {
        // Create an instance of the dialog fragment and show it
        TextView id = v.getRootView().findViewById(R.id.gameId);
        PropositionDialog dialog = new PropositionDialog(Integer.valueOf(id.getText().toString()));
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }
}