package fr.thomasp.secondlife.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import fr.thomasp.secondlife.models.Proposition;

@Dao
public interface PropositionDao {

    @Insert
    Long insertProposition(Proposition proposition);

    @Update
    int updateProposition(Proposition proposition);

    @Query("SELECT * from Proposition where id = :propositionId")
    Proposition getProposition(int propositionId);

    @Query("DELETE FROM Proposition WHERE id= :propositionId")
    int deleteProposition(int propositionId);
}
