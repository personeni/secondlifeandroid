package fr.thomasp.secondlife.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import fr.thomasp.secondlife.models.Announce;

@Dao
public interface AnnounceDao {

    @Insert
    Long insertAnnounce(Announce announce);

    @Update
    int updateAnnounce(Announce announce);

    @Query("SELECT * from Announce where id = :announceId")
    Announce getAnnounce(int announceId);

    @Query("DELETE FROM Announce WHERE id= :announceId")
    int deleteAnnounce(int announceId);
}
