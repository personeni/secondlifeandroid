package fr.thomasp.secondlife.database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import fr.thomasp.secondlife.database.dao.AnnounceDao;
import fr.thomasp.secondlife.database.dao.PropositionDao;
import fr.thomasp.secondlife.database.dao.UserDao;
import fr.thomasp.secondlife.models.Announce;
import fr.thomasp.secondlife.models.Proposition;
import fr.thomasp.secondlife.models.User;

@androidx.room.Database(entities = {User.class, Announce.class, Proposition.class}, version = 1, exportSchema = false)
public abstract class Database extends RoomDatabase {

    private static volatile Database INSTANCE;

    public abstract AnnounceDao announceDao();
    public abstract PropositionDao propositionDao();
    public abstract UserDao userDao();

    public static Database getInstance(Context context){
        if(INSTANCE == null){
            synchronized (Database.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), Database.class, "MyDatabase.db").build();
                }
            }
        }
        return INSTANCE;
    }
}
