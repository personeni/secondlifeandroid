package fr.thomasp.secondlife.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.thomasp.secondlife.models.User;

@Dao
public interface UserDao {
    @Insert
    long insertUser(User user);

    @Update
    int updateUser(User user);

    @Query("DELETE FROM User WHERE id= :userId")
    int deleteUser(int userId);

    @Query("SELECT * FROM User where id = :userId")
    User getUser(int userId);

}
