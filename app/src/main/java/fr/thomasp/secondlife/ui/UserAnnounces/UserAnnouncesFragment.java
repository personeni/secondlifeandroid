package fr.thomasp.secondlife.ui.UserAnnounces;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.Observable;

import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.api.ApiRequests;
import fr.thomasp.secondlife.models.Announce;
import fr.thomasp.secondlife.models.User;
import fr.thomasp.secondlife.ui.home.VideoGamesAdapter;


public class UserAnnouncesFragment extends Fragment {
    private MutableLiveData<ArrayList<Announce>> announces = new MutableLiveData<>();

    public UserAnnouncesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_user_announces, container, false);
        announces.setValue(new ArrayList<>());

        final androidx.lifecycle.Observer<ArrayList<Announce>> announcesObserver = new Observer<ArrayList<Announce>>(){

            @Override
            public void onChanged(ArrayList<Announce> announces) {
                RecyclerView recycler = (RecyclerView)root.findViewById(R.id.recycler_user_announces);
                recycler.setAdapter(new UserAnnouncesAdapter(getContext(),announces));
                recycler.setLayoutManager(new LinearLayoutManager(getContext()));
            }
        };
        announces.observe(getViewLifecycleOwner(), announcesObserver);
        ApiRequests.getInstance().getUserAnnounces(getContext(), announces);

        return root;
    }
}