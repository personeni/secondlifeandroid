package fr.thomasp.secondlife.ui.GameDetails;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.icu.text.UnicodeSetIterator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.api.ApiRequests;

public class PropositionDialog extends DialogFragment {
    private int id;
    private View v;
    public PropositionDialog(int id){
        this.id = id;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        v = inflater.inflate(R.layout.dialog_proposition, null);
        builder.setView(v)
                // Add action buttons
                .setPositiveButton(R.string.PositionPropositionModal, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        onclickConfirm();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
    public void onclickConfirm(){
        EditText amount = v.findViewById(R.id.amountEditText);
        ApiRequests.getInstance().createProposition(getContext(), id, Integer.valueOf(amount.getText().toString()));
    }
}
