package fr.thomasp.secondlife.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.models.Announce;
import fr.thomasp.secondlife.ui.GameDetails.GameDetails;

public class VideoGamesAdapter extends RecyclerView.Adapter<VideoGamesAdapter.ViewHolder> {

    private ArrayList<Announce> localDataSet;
    private Context context;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView descriptionTextView;
        private final TextView nameTextView;
        private final TextView priceTextView;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            descriptionTextView = (TextView) view.findViewById(R.id.videoGameDescriptionTextView);
            nameTextView = (TextView) view.findViewById(R.id.videoGameNameTextView);
            priceTextView = (TextView) view.findViewById(R.id.videoGamePriceTextView);
        }

        public TextView getDescriptionTextView() {
            return descriptionTextView;
        }
        public TextView getNameTextView(){
            return nameTextView;
        }
        public TextView getPriceTextView(){
            return priceTextView;
        }

    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used
     * by RecyclerView.
     */
    public VideoGamesAdapter(Context context, ArrayList<Announce> dataSet) {
        localDataSet = dataSet;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.video_games_row_item, viewGroup, false);

        return new ViewHolder(view);
    }

    public void updateData(ArrayList<Announce> announces)
    {
        localDataSet.clear();
        localDataSet.addAll(announces);
        notifyDataSetChanged();
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.getDescriptionTextView().setText(localDataSet.get(position).getDescription());
        viewHolder.getNameTextView().setText(localDataSet.get(position).getTitle());
        viewHolder.getPriceTextView().setText(String.valueOf(localDataSet.get(position).getPrice())+"€");
        viewHolder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                GameDetails fragment = new GameDetails(localDataSet.get(position).getId());
                FragmentTransaction  transaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.home_fragment,fragment, null)
                .addToBackStack(null)
                .commit();
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }
}