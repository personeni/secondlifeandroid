package fr.thomasp.secondlife.ui.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Date;

import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.api.ApiRequests;
import fr.thomasp.secondlife.databinding.ActivityLoginBinding;
import fr.thomasp.secondlife.databinding.FragmentHomeBinding;
import fr.thomasp.secondlife.models.Announce;
import fr.thomasp.secondlife.models.User;

import java.lang.reflect.Method;

public class HomeFragment extends Fragment {
    private FragmentHomeBinding binding;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();

        HomeViewModel model = new ViewModelProvider(this).get(HomeViewModel.class);
        RecyclerView recycler = binding.recyclerGamesHome;
        final VideoGamesAdapter adapter = new VideoGamesAdapter(getContext(), new ArrayList<Announce>());
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        model.getAnnounces(getContext()).observe(getViewLifecycleOwner(), announces -> {
            adapter.updateData(announces);
            binding.loader.setVisibility(View.INVISIBLE);
            binding.mainView.setVisibility(View.VISIBLE);
        });
        return root;
    }
}