package fr.thomasp.secondlife.ui.GameDetails;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.api.ApiRequests;
import fr.thomasp.secondlife.databinding.FragmentHomeBinding;
import fr.thomasp.secondlife.databinding.GameDetailsFragmentBinding;
import fr.thomasp.secondlife.models.Announce;
import fr.thomasp.secondlife.models.User;
import fr.thomasp.secondlife.ui.home.HomeViewModel;
import fr.thomasp.secondlife.ui.home.VideoGamesAdapter;
import fr.thomasp.secondlife.ui.login.LoginActivity;

public class GameDetails extends Fragment {

    private GameDetailsFragmentBinding binding;
    private int id = 0;

    public GameDetails(int id){
        this.id= id;
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        GameDetailsViewModel model = new ViewModelProvider(this).get(GameDetailsViewModel.class);

        binding = GameDetailsFragmentBinding.inflate(getLayoutInflater());
        View v = binding.getRoot();
        final SuggestionsAdapter adapter = new SuggestionsAdapter(getContext(), new ArrayList<Announce>());
        RecyclerView recycler = binding.recyclerViewGameDetails;
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(v.getContext(),LinearLayoutManager.HORIZONTAL, false));

        model.getAnnounce(getContext(), id).observe(getViewLifecycleOwner(), announce -> {
            binding.textViewGameName.setText(announce.getTitle());
            binding.textViewGameDescription.setText(announce.getDescription());
            binding.GamePriceTextView.setText(announce.getPrice()+"€");
            binding.textViewSeller.setText(announce.getCreator().getUserName());
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            binding.creationDateView.setText(df.format(announce.getCreatedDate()));
            binding.gameId.setText(String.valueOf(announce.getId()));
            binding.creatorId.setText(String.valueOf(announce.getCreator().getId()));
            binding.detailsLoading.setVisibility(View.INVISIBLE);
            binding.mainContent.setVisibility(View.VISIBLE);
        });

        model.getSuggestions(getContext()).observe(getViewLifecycleOwner(), suggestions -> {
            adapter.updateData(suggestions);
            binding.suggestionsLoader.setVisibility(View.INVISIBLE);
            binding.recyclerViewGameDetails.setVisibility(View.VISIBLE);
        });

        binding.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float newRating, boolean fromUser) {
                String creatorID = binding.creatorId.getText().toString();
                ApiRequests.getInstance().rateUser(getContext(), (int) newRating, Integer.valueOf(creatorID));
            }
        });
        return v;
    }

}