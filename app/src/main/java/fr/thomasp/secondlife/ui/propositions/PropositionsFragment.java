package fr.thomasp.secondlife.ui.propositions;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;

import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.models.Announce;
import fr.thomasp.secondlife.models.Proposition;
import fr.thomasp.secondlife.models.User;
import fr.thomasp.secondlife.ui.GameDetails.SuggestionsAdapter;
import fr.thomasp.secondlife.ui.home.VideoGamesAdapter;

public class PropositionsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_propositions, container, false);

        RecyclerView recycler = (RecyclerView)root.findViewById(R.id.recyclerPropositions);

        Announce annouce1 = new Announce(1, "jeux 1", "une super description", 25.35, new User(1,  "Quentin", "Schaeffer", "superPseudo", "0608276198"), new Date());
        Announce announce2 = new Announce(2,"jeux 2", "une autre super description", 45.89, new User(2,"Fiona", "Facchini", "pseudo", "0608276198"), new Date());
        Announce announce3 = new Announce(3,"jeux 3", "une description au tacquet", 68.75, new User(3,"Blabla", "Testas","testastos", "0608276198"), new Date());

        User user1 = new User(1,"Jean","Eude","JeanEudeDU67","0608276198");
        User user2 = new User(2,"Kevin","du67","Xx_KevinDu67BGGamer_xX", "0608276198");
        User user3 = new User(3,"Jordan","Collof","JordanBgDeCallof","0608276198");
        Proposition[] propositions = new Proposition[]{
                new Proposition(999,annouce1, 43.5, user1),
                new Proposition(998,announce2,58.5,user2),
                new Proposition(997,announce3, 75.9, user3)
        };
        recycler.setAdapter(new PropositionsAdapter(getContext(),propositions));
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        return root;
    }
}