package fr.thomasp.secondlife.ui.GameDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.models.Announce;

public class SuggestionsAdapter extends RecyclerView.Adapter<fr.thomasp.secondlife.ui.GameDetails.SuggestionsAdapter.ViewHolder> {

    private ArrayList<Announce> localDataSet;
    private Context context;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView;
        private final TextView priceTextView;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            nameTextView = (TextView) view.findViewById(R.id.suggestionTitle);
            priceTextView = (TextView) view.findViewById(R.id.suggestionPrice);
        }

        public TextView getNameTextView(){
            return nameTextView;
        }
        public TextView getPriceTextView(){
            return priceTextView;
        }

    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used
     * by RecyclerView.
     */
    public SuggestionsAdapter(Context context, ArrayList<Announce> dataSet) {
        localDataSet = dataSet;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public fr.thomasp.secondlife.ui.GameDetails.SuggestionsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.suggestion_row_item, viewGroup, false);

        return new fr.thomasp.secondlife.ui.GameDetails.SuggestionsAdapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(fr.thomasp.secondlife.ui.GameDetails.SuggestionsAdapter.ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.getNameTextView().setText(localDataSet.get(position).getTitle());
        viewHolder.getPriceTextView().setText(String.valueOf(localDataSet.get(position).getPrice())+"€");
        viewHolder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                GameDetails fragment = new GameDetails(localDataSet.get(position).getId());
                FragmentTransaction transaction = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.game_details_fragment,fragment, null)
                        .addToBackStack(null)
                        .commit();
            }
        });

    }

    public void updateData(ArrayList<Announce> announces)
    {
        localDataSet.clear();
        localDataSet.addAll(announces);
        notifyDataSetChanged();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }
}
