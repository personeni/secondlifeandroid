package fr.thomasp.secondlife.ui.home;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import fr.thomasp.secondlife.api.ApiRequests;
import fr.thomasp.secondlife.models.Announce;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<ArrayList<Announce>> announces;

    public LiveData<ArrayList<Announce>> getAnnounces(Context context) {
        if(announces == null)
        {
            announces = new MutableLiveData<ArrayList<Announce>>();
            loadAnnounces(context);
        }
        return announces;
    }

    private void loadAnnounces(Context context)
    {
        ApiRequests.getInstance().getListAnnounce(context, announces);
    }
}