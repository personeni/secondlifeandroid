package  fr.thomasp.secondlife.ui.login;

import androidx.annotation.RequiresApi;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import fr.thomasp.secondlife.MainActivity;
import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.api.ApiRequests;
import fr.thomasp.secondlife.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        MutableLiveData<String> resultLogin = new MutableLiveData<>();
        final Observer<String> observer = new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.loading.setVisibility(View.INVISIBLE);
                if(!s.isEmpty()){
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    binding.errorMessage.setVisibility(View.VISIBLE);
                    binding.errorMessage.setText(s);
                }
            }
        };
        resultLogin.observe(this, observer);
        binding.password.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    ApiRequests.getInstance().login(getApplicationContext(), binding.username.getText().toString(), binding.password.getText().toString(), resultLogin);
                }
                return false;
            }
        });

        binding.login.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                binding.errorMessage.setVisibility(View.INVISIBLE);
                binding.loading.setVisibility(View.VISIBLE);
                ApiRequests.getInstance().login(getApplicationContext(), binding.username.getText().toString(), binding.password.getText().toString(), resultLogin);
            }
        });
    }

}