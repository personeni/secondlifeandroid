package fr.thomasp.secondlife.ui.GameDetails;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import fr.thomasp.secondlife.api.ApiRequests;
import fr.thomasp.secondlife.models.Announce;

public class GameDetailsViewModel extends ViewModel {
    private MutableLiveData<Announce> announce;
    private MutableLiveData<ArrayList<Announce>> suggestions;

    public MutableLiveData<Announce> getAnnounce(Context context, int id)
    {
        if(announce == null)
        {
            announce = new MutableLiveData<Announce>();
            loadAnnounce(context, id);
        }
        return announce;
    }

    public MutableLiveData<ArrayList<Announce>> getSuggestions(Context context)
    {
        if(suggestions == null)
        {
            suggestions = new MutableLiveData<ArrayList<Announce>>();
            loadSuggestions(context);
        }
        return suggestions;
    }

    private void loadAnnounce(Context context, int id)
    {
        ApiRequests.getInstance().getAnnounceDetails(context, announce, id);
    }

    private void loadSuggestions(Context context)
    {
        ApiRequests.getInstance().getRecommendedGames(context, suggestions);
    }

}