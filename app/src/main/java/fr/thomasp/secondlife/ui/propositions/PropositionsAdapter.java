package fr.thomasp.secondlife.ui.propositions;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import fr.thomasp.secondlife.R;
import fr.thomasp.secondlife.models.Announce;
import fr.thomasp.secondlife.models.Proposition;
import fr.thomasp.secondlife.ui.GameDetails.GameDetails;

public class PropositionsAdapter extends RecyclerView.Adapter<fr.thomasp.secondlife.ui.propositions.PropositionsAdapter.ViewHolder> {

    private Proposition[] localDataSet;
    private Context context;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView;
        private final TextView priceTextView;
        private final ImageButton confirmationButton;
        private final ImageButton refuseButton;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            nameTextView = view.findViewById(R.id.propositionGameTitle);
            priceTextView = view.findViewById(R.id.propositionPrice);
            confirmationButton = view.findViewById(R.id.propositionConfirmationButton);
            refuseButton = view.findViewById(R.id.propositionCancelButton);
        }

        public TextView getNameTextView(){
            return nameTextView;
        }
        public TextView getPriceTextView(){
            return priceTextView;
        }
        public ImageButton getConfirmationButton(){
            return confirmationButton;
        }
        public ImageButton getRefuseButton(){
            return refuseButton;
        }

    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used
     * by RecyclerView.
     */
    public PropositionsAdapter(Context context, Proposition[] dataSet) {
        localDataSet = dataSet;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public fr.thomasp.secondlife.ui.propositions.PropositionsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.proposition_row_item, viewGroup, false);

        return new fr.thomasp.secondlife.ui.propositions.PropositionsAdapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(fr.thomasp.secondlife.ui.propositions.PropositionsAdapter.ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.getNameTextView().setText(localDataSet[position].getAnnounce().getTitle());
        viewHolder.getPriceTextView().setText(String.valueOf(localDataSet[position].getPrice())+"€");
        viewHolder.getConfirmationButton().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.d("test","on est laaa");
                DialogFragment newFragment = new PropositionConfirmationModal();
                newFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "propositionConfirmation");
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return localDataSet.length;
    }
}
