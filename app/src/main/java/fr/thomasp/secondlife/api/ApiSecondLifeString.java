package fr.thomasp.secondlife.api;

import android.util.Log;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class ApiSecondLifeString extends StringRequest {
    private String token;
    private String param;

    public ApiSecondLifeString(int method, String url, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener,String token, String param) {
        super(method, url, listener, errorListener);
        this.token = token;
        this.param = param;
    }

    public ApiSecondLifeString(String url, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener,String token, String param) {
        super(url, listener, errorListener);
        this.token = token;
        this.param = param;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Log.d("tokenStringREquest", token);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Bearer "+token);
        return headers;
    }
    @Override
    protected Map<String, String> getParams()
    {
        Map<String, String>  params = new HashMap<String, String>();
        params.put("price", param);
        return params;
    }
}
