package fr.thomasp.secondlife.api;

import android.content.Context;
import android.drm.DrmInfoRequest;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import fr.thomasp.secondlife.models.Announce;
import fr.thomasp.secondlife.models.Proposition;
import fr.thomasp.secondlife.models.User;
import kotlin.jvm.internal.MutablePropertyReference;

public class ApiRequests {
    private String baseUrl = "https://secondlife20210324175105.azurewebsites.net/";
    private String announceList = baseUrl+"api/announce/list";
    private String login = baseUrl+"api/user/login";
    private String urlAnnounceDetails = baseUrl+"api/announce/{id}";
    private String urlCreateAnnounce = baseUrl+"api/announce";
    private String urlRecommandedGames = baseUrl+"api/announce/suggestions/5";
    private String urlCreateProposition = baseUrl+"api/announce/{id}/makeProposal";
    private String urlGetReceivedProposals =  baseUrl+"api/user/proposals/received";
    private String urlUserAnnounces = baseUrl+"api/user/announces";
    private String urlRemoveAnnounce=  baseUrl+"api/announce/{id}";
    private String urlRateUser = baseUrl+"api/user/{id}/rate";

    private String token = "";

    private static ApiRequests instance;

    private ApiRequests(){

    }
    public static ApiRequests getInstance(){
        if(instance != null){
            return instance;
        }else{
            instance = new ApiRequests();
            return instance;
        }
    }

    public void getAnnounceDetails(final Context context, MutableLiveData mutable, Integer id){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        Log.d("api", urlAnnounceDetails.replace("{id}", String.valueOf(id)));
        ApiSecondLifeObjects api = new ApiSecondLifeObjects(ApiSecondLifeObjects.Method.GET, urlAnnounceDetails.replace("{id}", String.valueOf(id)), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("debug res", response.getString("title"));
                    Date creationDate = null;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        creationDate = simpleDateFormat.parse(response.getString("createdDate").split("T")[0]);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    JSONObject creatorJson = response.getJSONObject("creator");
                    User creator = new User(
                            creatorJson.getInt("id"),
                            creatorJson.getString("firstName"),
                            creatorJson.getString("name"),
                            creatorJson.getString("userName"),
                            creatorJson.getString("phone")
                    );
                    Announce announce = new Announce(
                            response.getInt("id"),
                            response.getString("title"),
                            response.getString("description"),
                            response.getDouble("price"),
                            creator,
                            creationDate
                    );
                    mutable.setValue(announce);
                    Log.d("api",announce.getTitle());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("apierror","error");
                error.printStackTrace();
            }
        }, token);
        mRequestQueue.add(api);
    }

    public void getListAnnounce(final Context context, MutableLiveData mutable){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        ApiSecondLifeArray api = new ApiSecondLifeArray(ApiSecondLifeArray.Method.GET,announceList, null, new Response.Listener<JSONArray >() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    ArrayList<Announce> announces = new ArrayList<>();
                    for(int i=0;i<response.length();i++){
                        JSONObject jresponse = response.getJSONObject(i);
                        String title = jresponse.getString("title");
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date creationDate = null;
                        try {
                            creationDate = simpleDateFormat.parse(jresponse.getString("createdDate").split("T")[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Announce announce = new Announce(jresponse.getInt("id"), jresponse.getString("title"), jresponse.getString("description"), jresponse.getDouble("price"),null,creationDate);
                        announces.add(announce);
                    }
                    mutable.setValue(announces);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Erreur lors de la récupération des annonces", Toast.LENGTH_SHORT).show();
                Log.d("erreur recup","");
            }
        }, token);
        mRequestQueue.add(api);
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void login(final Context context, String username, String password, MutableLiveData mutable){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);

        JSONObject jsonBody = new JSONObject();
        try{
            jsonBody.put("username", username);
            jsonBody.put("password", password);
        }catch(Exception e ){
            Log.d("APIERREUR", "erreur creation json");
        }

        JsonObjectRequest req = new JsonObjectRequest(JsonObjectRequest.Method.POST, login, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    mutable.setValue(response.getString("token"));
                    token = response.getString("token");
                } catch (JSONException e) {
                    mutable.setValue("Erreur: Une erreur inattendue est survenue.");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error.networkResponse.statusCode == 400)
                {
                    mutable.setValue("Erreur: Identifiants incorrects.");
                }
                else
                {
                    mutable.setValue("Erreur: Une erreur inattendue est survenue.");
                }

            }
        });
        mRequestQueue.add(req);
    }

    public void createAnnounce(final Context context, String title, String description, String price, MutableLiveData mutable){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);

        JSONObject json = new JSONObject();
        try {
            json.put("title", title);
            json.put("price", Integer.valueOf(price));
            json.put("description",description);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiSecondLifeObjects req = new ApiSecondLifeObjects(ApiSecondLifeObjects.Method.POST, urlCreateAnnounce, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mutable.setValue(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mutable.setValue(false);
                error.printStackTrace();
            }
        }, token);
        mRequestQueue.add(req);
    }
    public void getRecommendedGames(final Context context, MutableLiveData mutable){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        ApiSecondLifeArray req = new ApiSecondLifeArray(ApiSecondLifeArray.Method.GET, urlRecommandedGames, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    ArrayList<Announce> announces = new ArrayList<>();
                    for(int i=0;i<response.length();i++){
                        JSONObject jresponse = response.getJSONObject(i);
                        String title = jresponse.getString("title");
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date creationDate = null;
                        try {
                            creationDate = simpleDateFormat.parse(jresponse.getString("createdDate").split("T")[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Announce announce = new Announce(jresponse.getInt("id"), jresponse.getString("title"), jresponse.getString("description"), jresponse.getDouble("price"),null,creationDate);
                        announces.add(announce);
                    }
                    mutable.setValue(announces);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }, token);

        mRequestQueue.add(req);
    }
    public void createProposition(final Context context, int announceId, int proposalAmount){
        RequestQueue mRequestqueue = Volley.newRequestQueue(context);

        JSONObject json = new JSONObject();
        try {
            json.put("price", proposalAmount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiSecondLifeString req = new ApiSecondLifeString(ApiSecondLifeString.Method.POST, urlCreateProposition.replace("{id}", String.valueOf(announceId)), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, token, String.valueOf(proposalAmount));
        mRequestqueue.add(req);
    }
    public void getUserProposalsReceived(final Context context, MutableLiveData mutable){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);

        ApiSecondLifeArray req = new ApiSecondLifeArray(ApiSecondLifeArray.Method.GET, urlGetReceivedProposals, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ArrayList<Proposition> propositions = new ArrayList<>();
                for(int i=0;i<response.length();i++){
                    JSONObject jresponse = null;
                    try {
                        jresponse = response.getJSONObject(i);
                        JSONObject userJson = jresponse.getJSONObject("proposer");
                        User proposer = new User(userJson.getInt("id"), userJson.getString("firstName") , userJson.getString("name"), userJson.getString("userName"), userJson.getString("phone"));

                        JSONObject announceJson = jresponse.getJSONObject("annonce");
                        JSONObject userCreatorJson = announceJson.getJSONObject("creator");
                        User creator = new User(userCreatorJson.getInt("id"), userCreatorJson.getString("firstName"), userCreatorJson.getString("name"), userCreatorJson.getString("userName"), userCreatorJson.getString("phone"));

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date creationDate = null;

                        try {
                            creationDate = simpleDateFormat.parse(announceJson.getString("createdDate").split("T")[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Announce announce = new Announce(announceJson.getInt("id"), announceJson.getString("title"), announceJson.getString("description"),announceJson.getDouble("price"), creator, creationDate);

                        Proposition proposition = new Proposition(jresponse.getInt("id"),announce, jresponse.getDouble("price"), proposer);
                        propositions.add(proposition);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                mutable.setValue(propositions);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }, token);
        mRequestQueue.add(req);
    }
    public void getUserAnnounces(final Context context, MutableLiveData mutable){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        ApiSecondLifeArray req = new ApiSecondLifeArray(ApiSecondLifeArray.Method.GET, urlUserAnnounces, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    ArrayList<Announce> announces = new ArrayList<>();
                    for(int i=0;i<response.length();i++){
                        JSONObject jresponse = response.getJSONObject(i);
                        String title = jresponse.getString("title");
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date creationDate = null;
                        try {
                            creationDate = simpleDateFormat.parse(jresponse.getString("createdDate").split("T")[0]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Announce announce = new Announce(jresponse.getInt("id"), jresponse.getString("title"), jresponse.getString("description"), jresponse.getDouble("price"),null,creationDate);
                        announces.add(announce);
                    }
                    mutable.setValue(announces);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, token);
        mRequestQueue.add(req);
    }
    public void removeAnnounce(final Context context, int announceId){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);

        ApiSecondLifeObjects req = new ApiSecondLifeObjects(ApiSecondLifeObjects.Method.DELETE, urlRemoveAnnounce.replace("{id}", String.valueOf(announceId)), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, token);
        mRequestQueue.add(req);
    }
    public void rateUser(final Context context, int rate, int userId){
        RequestQueue mRequestQueue = Volley.newRequestQueue(context);
        JSONObject json = new JSONObject();
        try {
            json.put("note", rate);
            json.put("description","");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiSecondLifeObjects req = new ApiSecondLifeObjects(ApiSecondLifeObjects.Method.POST, urlRateUser.replace("{id}", String.valueOf(userId)), json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }, token);
        mRequestQueue.add(req);
    }
    public void getUser(final Context context, MutableLiveData mutable){

    }
}
